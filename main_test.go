package main

import (
	"testing"

	"github.com/bmizerany/assert"
)

func Test_increment(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		x := increment(5)

		assert.Equal(t, 6, x)
	})

}
