package main

import "fmt"

func main() {
	fmt.Println(increment(5))
}

func increment(a int) int {
	return a + 1
}
